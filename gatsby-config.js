/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

if (process.env.NODE_ENV === "development") {
  require("dotenv").config();
}

module.exports = {
  /* Your site config here */
  siteMetadata: {
    title: "Александр Гришанин",
    description: "Александр Гришанин - Блог",
    siteUrl: "https://grixal.ru/"
  },
  plugins: [
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Александр Гришанин - Блог`,
        short_name: `Александр Гришанин`,
        lang: `ru`,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#343a40`,
        display: `standalone`,
        icon: `static/favicon.png`,
        include_favicon: true
      }
    },
    {
      resolve: `gatsby-plugin-offline`,
      options: {
        precachePages: ["/blog/*", "/works/*"]
      }
    },
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: process.env.CONTENTFUL_SPACE_ID,
        accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
        downloadLocal: true
      }
    },
    {
      resolve: "gatsby-plugin-feed",
      options: {
        query: `
          {
            site {
              siteMetadata {
                title
                description
                siteUrl
                site_url: siteUrl
              }
            }
          }
        `,
        feeds: [
          {
            serialize: ({ query: { site, allContentfulArticle } }) => {
              return allContentfulArticle.edges.map(edge => {
                return {
                  title: edge.node.title,
                  description: edge.node.description,
                  date: edge.node.publishedAt,
                  url: site.siteMetadata.siteUrl + '/post/' + edge.node.link,
                  guid: site.siteMetadata.siteUrl + '/post/' + edge.node.link
                };
              });
            },
            query: `
            {
              allContentfulArticle {
                edges {
                  node {
                    link
                    description
                    title
                    publishedAt
                  }
                }
              }
            }
            `,
            output: "/rss.xml"
          }
        ]
      }
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sitemap`,
    `gatsby-plugin-robots-txt`,
    `gatsby-plugin-sass`
  ]
}
