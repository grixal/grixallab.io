# [Personal page](https://grixal.gitlab.io) on GitLab Pages

Пет-проект персональной страницы на Gitlab Pages.

## 🧐 Используемые технологии

- [React.js](https://reactjs.org) - UI фреймворк для разработки клиетских приложений.
- [GarsbyJS](https://www.gatsbyjs.org) - генератор статических сайтов написанный. Превращает _React.js_ приложения в статический код.
- [Contentful](https://www.contentful.com) - новая API-ориентированная система управления контентом. Позволяет получать контент для сайта с помощью _GraphQL_ запросов.

## 🚀 Quick start

1.  **Install Gatsby CLI**

    ```shell
    npm i gatsby-cli
    ```
    
2.  **Git Clone**

    ```shell
    https://gitlab.com/grixal/grixal.gitlab.io.git
    ```

3.  **Run on developer mode**

    ```shell
    cd grixal.gitlab.io/
    gatsby develop
    ```

4.  **Open site**

    Сайт будет запущен по адресуу `http://localhost:8000`!

    _Note: Вы увидите вторую ссылку: _`http://localhost:8000/___graphql`_. Этот инструмент мы можете использовать для экспериментов с запросами данных [Gatsby GraphQL Tutorial](https://www.gatsbyjs.org/tutorial/part-five/#introducing-graphiql)._
    

## 💫 Deploy

Сборка приложения происходит через **Gitlab CI** с использованием _GatsbyCLI_. После сборки приложение размещается на хостинге **GitLab**.
    
_gitlab-ci.yml_

    image: node:latest
    
    cache:
      paths:
        - node_modules/
        - .cache/
        - public/
    pages:
      script:
        - npm install
        - ./node_modules/.bin/gatsby build --prefix-paths
      artifacts:
        paths:
          - public
      only:
        - master