import React from "react";
import { Helmet } from "react-helmet";
import { graphql } from "gatsby";
import {About, Categories, Footer, Header, Tags} from "../components/components";
import { Col, Container, Row } from "react-bootstrap";
import Article from "../templates/article";

const Index = ({data: {config, tags, categories, items: { posts }}}) => {
    return (
      <main>
        <Helmet
            title={`${config.title}`}
            htmlAttributes={{lang: "ru"}}
            defer={false}
            meta={[
                {"name": "google-site-verification", "content": "Ys44dZhR2KrTCL3zxKSckTGc4CsWChXGnVJryyfdGlY"},
                {"name": "yandex-verification", "content": "fe1b0df38f1038f5"}
            ]}
        />
          <Header config={config}/>
          <div className="recent">
            <Container>
              <About profile={config.profile}/>
              <Row className="recent-inner">
                  <Col md="8" sm="8" xs="12">
                      <h1> Последние записи </h1>
                      {posts.map(item => <Article item={item.node}/>)}
                  </Col>
                  <Col md="4" sm="4" xs="12">
                      <Tags tags={tags}/>
                      <Categories categories={categories} />
                  </Col>
              </Row>
            </Container>
          </div>
        <Footer config={config}/>
      </main>
    )
}

export const query = graphql`{
  items: allContentfulArticle(limit: 5, sort: {fields: publishedAt, order: DESC}) {
    posts: edges {
      node {
        title
        description
        link
        publishedAt
        tags
        categories
      }
    }
  }
  tags: allContentfulArticle {
    group(field: tags) {
      value: fieldValue
      pageInfo {
        itemCount
      }
    }
  }
  categories: allContentfulArticle {
    group(field: categories) {
      value: fieldValue
      pageInfo {
        itemCount
      }
    }
  }
  config: contentfulConfig {
    title
    profile {
      fullName
      description
      bio {
        bio
      }
      avatar {
        fixed(width: 200) {
          srcWebp
        }
      }
      links {
        group
        icon
        link
      }
    }
    logo {
      fixed(width: 38) {
        srcWebp
      }
    }
  }
}`

export default Index