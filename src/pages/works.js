import React from "react";
import { graphql, Link } from "gatsby";
import { Footer, Header } from "../components/components";
import { Col, Container, Row, CardColumns, Card } from "react-bootstrap";

import img from "../../static/img/undraw_quiz_nlyh.png";
import { Helmet } from "react-helmet";

const Works = ({data: {config, items: { works }}}) => {
  return (
    <div>
      <Helmet title={`${config.title} - Проекты`} htmlAttributes={{lang: "ru"}} defer={false} />
      <Header config={config}/>
      <main className="recent">
        <Container>
          <Row>
            <Col md="6" sm="12" xs="12">
              <div className="list-show-title">
                <h1>These are my works</h1>
              </div>
            </Col>
            <Col md="6" sm="12" xs="12" style={{ textAlign: "center" }}>
              <img src={img} className="img-fluid" alt="" />
            </Col>
          </Row>
          <Row className="recent-inner">
            <CardColumns style={{width: "100%"}}>
              {works.map(({ node }) => {
                return (
                  <Card className="mx-auto" >
                    {node.preview && <Card.Img variant="top" src={node.preview.fluid.srcWebp}/>}
                    <Card.Body>
                      <Card.Title>{node.title}</Card.Title>
                      <Card.Text>{node.description}</Card.Text>
                      <Link className="btn btn-secondary" to={"work/" + node.link}>Читать дальше</Link>
                    </Card.Body>
                  </Card>
                )
              })}
            </CardColumns>
          </Row>
        </Container>
      </main>
      <Footer config={config}/>
    </div>
  )
}

export const query = graphql`{
  items: allContentfulProject {
    works: edges {
      node {
        description
        title
        link
        preview {
          fluid(maxWidth: 400) {
            srcWebp
          }
        } 
      }
    }
  }
  config: contentfulConfig {
    title
    logo {
      fixed(width: 38) {
        srcWebp
      }
    }
  }
}`

export default Works