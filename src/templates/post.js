import React from "react";
import { graphql } from "gatsby";
import ReactMarkdown from "react-markdown";
import { Card, Col, Container, Row } from "react-bootstrap";
import { Footer, Header, PostInfo, RecentPosts } from "../components/components";
import { Helmet } from "react-helmet";
import CodeBlock from "../components/code-block";
import ImgBlock from "../components/img-block";

const Post = ({ data: { config, post, recent } }) => {
  return (
    <div>
      <Helmet
        htmlAttributes={{lang: "ru"}}
        title={post.title}
        meta={[
          {"name": "description", "content": [config.title, post.categories, post.tags]}
        ]}
        defer={false}
      />
      <Header config={config}/>
      <main className="recent">
        <Container className="mb-3">
          <Row>
            <Col md="8" sm="12" xs="12">
              <Card className="single-card">
                <br/>
                <blockquote className="self-elem-quote self-elem-quote-bg-blue markdown-body single-title" >
                  <h2>{ post.title }</h2>
                  <PostInfo item={post}/>
                </blockquote>
                {post.banner && <Card.Img variant="top" src={post.banner.fluid.srcWebp}/>}
                <Card.Body>
                  <ReactMarkdown source={post.text.text} escapeHtml={false} renderers={{code: CodeBlock, image: ImgBlock}}/>
                </Card.Body>
              </Card>
            </Col>
            <Col md="4" sm="12" xs="12">
              <RecentPosts posts={recent}/>
            </Col>
          </Row>
        </Container>
      </main>
      <Footer config={config}/>
    </div>
  )
}

export const query = graphql`
query ($slug: String!) {
  post: contentfulArticle(link: {eq: $slug}) {
    description
    title
    link
    banner {
      fluid(maxWidth: 900) {
        srcWebp
      }
    }
    text {
      text
    }
    publishedAt
    tags
    categories
  }
  recent: allContentfulArticle(limit: 5, sort: {fields: publishedAt, order: DESC}) {
    edges {
      node {
        id
        title
        link
        description
        publishedAt
        tags
        categories
      }
    }
  }
  config: contentfulConfig {
    title
    logo {
      fixed(width: 38) {
        srcWebp
      }
    }
  }
}
`

export default Post