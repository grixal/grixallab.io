import React from "react";
import { Link } from "gatsby";
import { PostInfo } from "../components/components";

const Article = ({ item }) => {
  return (
    <blockquote className="self-elem-quote self-elem-quote-bg-blue mt-2" style={{backgroundColor: '#FFFFFF'}}>
      <Link to={"/post/" + item.link}>
        <h2>{item.title}</h2>
      </Link>
      <PostInfo item={item} />
      <h5>
        {item.description}
      </h5>
    </blockquote>
  )
}

export default Article