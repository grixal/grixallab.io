import React from "react";
import { graphql } from "gatsby";
import {Categories, Footer, Header, Tags} from "../components/components";
import { Col, Container, Row } from "react-bootstrap";
import Article from "./article";

import img from "../../static/img/undraw_chat_bot_kli5.png";
import { Helmet } from "react-helmet";

const PostList = ({ data: { config, tags, categories, items: { posts } }, pageContext }) => {
  return (
    <div>
      <Helmet
        htmlAttributes={{lang: "ru"}}
        title={`${config.title} - ${pageContext.slug}`}
        meta={[
          {"name": "description", "content": [config.title, pageContext.slug]}
        ]}
      />
      <Header config={config}/>
      <main className="recent">
        <Container>
          <Row>
            <Col md="6" sm="12" xs="12">
              <div className="list-show-title">
                <h1>These are my blogs</h1>
                <h2>and also my life</h2>
              </div>
            </Col>
            <Col md="6" sm="12" xs="12" style={{ textAlign: "center" }}>
              <img src={img} className="img-fluid" alt=""/>
            </Col>
          </Row>
          <Row className="recent-inner">
              <Col md="8" sm="8" xs="12">
                  <h1> Последние записи </h1>
                  {posts.map(item => <Article item={item.node} />)}
              </Col>
              <Col md="4" sm="4" xs="12">
                  <Tags tags={tags}/>
                  <Categories categories={categories} />
              </Col>
          </Row>
        </Container>
      </main>
      <Footer config={config}/>
    </div>
  );
};

export const query = graphql`
query ($qualifier: ContentfulArticleFilterInput!) {
  items: allContentfulArticle(filter: $qualifier) {
    posts: edges {
      node {
        title
        link
        description
        publishedAt
        tags
        categories
      }
    }
  }
  tags: allContentfulArticle {
    group(field: tags) {
      value: fieldValue
      pageInfo {
        itemCount
      }
    }
  }
  categories: allContentfulArticle {
    group(field: categories) {
      value: fieldValue
      pageInfo {
        itemCount
      }
    }
  }
  config: contentfulConfig {
    title
    logo {
      fixed(width: 38) {
        srcWebp
      }
    }
  }
}`;

export default PostList;