import React from "react";
import { graphql } from "gatsby";
import {Footer, Header, Tags, Pagination, Categories} from "../components/components";
import { Col, Container, Row } from "react-bootstrap";
import Article from "./article";

import img from "../../static/img/undraw_chat_bot_kli5.png";
import { Helmet } from "react-helmet";

const Blog = ({data: {config, tags, categories, items: { posts }}, pageContext}) => {
  return (
    <main>
      <Helmet title={`${config.title} - Блог`} htmlAttributes={{lang: "ru"}} defer={false} />
      <Header config={config}/>
      <div className="recent">
        <Container>
          <Row>
            <Col md="6" sm="12" xs="12">
              <div className="list-show-title">
                <h1>These are my blogs</h1>
                <h2>and also my life</h2>
              </div>
            </Col>
            <Col md="6" sm="12" xs="12" style={{ textAlign: "center" }}>
              <img src={img} className="img-fluid" alt="" />
            </Col>
          </Row>
          <Row className="recent-inner">
            <Col md="8" sm="8" xs="12">
              {posts.map(item => <Article item={item.node}/>)}
              <Pagination pageContext={pageContext} prefix="blog"/>
            </Col>
            <Col md="4" sm="4" xs="12">
              <Tags tags={tags}/>
              <Categories categories={categories} />
            </Col>
          </Row>
        </Container>
      </div>
      <Footer config={config}/>
    </main>
  )
}

export const query = graphql`
query ($skip: Int!, $limit: Int!) {
  items: allContentfulArticle(skip: $skip, limit: $limit, sort: {fields: publishedAt, order: DESC}) {
    posts: edges {
      node {
        title
        link
        description
        publishedAt
        tags
        categories
      }
    }
  }
  tags: allContentfulArticle {
    group(field: tags) {
      value: fieldValue
      pageInfo {
        itemCount
      }
    }
  }
  categories: allContentfulArticle {
    group(field: categories) {
      value: fieldValue
      pageInfo {
        itemCount
      }
    }
  }
  config: contentfulConfig {
    title
    logo {
      fixed(width: 38) {
        srcWebp
      }
    }
  }
}`

export default Blog