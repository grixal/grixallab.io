import React from "react";
import { graphql } from "gatsby";
import ReactMarkdown from "react-markdown";
import { Card, Col, Container, Row } from "react-bootstrap";
import { Footer, Header, RecentPosts } from "../components/components";
import { Helmet } from "react-helmet";
import CodeBlock from "../components/code-block";
import ImgBlock from "../components/img-block";

const Work = ({ data: { config, work, recent } }) => {
  return (
    <div>
      <Helmet
        htmlAttributes={{lang: "ru"}}
        title={work.title}
        meta={[
          {"name": "description", "content": [config.title, work.title]}
        ]}
        defer={false}
      />
      <Header config={config}/>
      <main className="recent">
        <Container className="mb-3">
          <Row>
            <Col md="8" sm="12" xs="12">
              <Card className="single-card">
                <Card.Header><h2>{ work.title }</h2></Card.Header>
                {work.preview && <Card.Img variant="top" src={work.preview.fluid.srcWebp}/>}
                <Card.Body>
                  <ReactMarkdown source={work.text.text} escapeHtml={false} renderers={{code: CodeBlock, image: ImgBlock}}/>
                </Card.Body>
              </Card>
            </Col>
            <Col md="4" sm="12" xs="12">
              <RecentPosts posts={recent}/>
            </Col>
          </Row>
        </Container>
      </main>
      <Footer config={config}/>
    </div>
  )
}

export const query = graphql`
query ($slug: String!) {
  work: contentfulProject(link: {eq: $slug}) {
    link
    description
    title
    text {
      text
    }
    preview {
      fluid(maxWidth: 900) {
          srcWebp
      }
    } 
  }
  recent: allContentfulArticle(limit: 5, sort: {fields: publishedAt, order: DESC}) {
    edges {
      node {
        title
        link
        description
        publishedAt
        tags
        categories
      }
    }
  }
  config: contentfulConfig {
    title
    logo {
      fixed(width: 38) {
        srcWebp
      }
    }
  }
}
`

export default Work