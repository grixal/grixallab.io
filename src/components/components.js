import React from "react";
import moment from "moment";
import _ from "lodash";
import {Link} from "gatsby";
import {Card, Col, Container, Nav, Navbar, Row} from "react-bootstrap";
import {config, library} from '@fortawesome/fontawesome-svg-core'
import {fab} from '@fortawesome/free-brands-svg-icons';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {far} from '@fortawesome/free-regular-svg-icons';

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import img from "../../static/img/undraw_chat_bot_kli5.png";
import ReactMarkdown from "react-markdown";
import {YMInitializer} from 'react-yandex-metrika';

config.autoAddCss = false
library.add(fab)
library.add(fas)
library.add(far)

export const Header = ({ config }) => {
  return (
    <Navbar bg="dark" variant="dark" expand="lg">
      <Container fluid>
        <Navbar.Brand href={"/"}>
          <img srcSet={config.logo.fixed.srcWebp} height="38px" alt=""/>
          <Navbar.Text className="ml-2">{config.title}</Navbar.Text>
        </Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Nav style={{fontSize: '1.25em'}}>
            <Link className="nav-link" to={"/"}>Главная</Link>
            <Link className="nav-link" to={"/blog"}>Блог</Link>
            <Link className="nav-link" to={"/works"}>Проекты</Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export const Footer = ({ config }) => {
  return (
    <footer>
        <YMInitializer accounts={[65419996]}/>
      <Container>
        <p className="copyright">{config.copyright}</p>
      </Container>
    </footer>
  )
}

export const About = ({ profile }) => {
  return (
    <Row>
      <Col xs="12" sm="4" md="4" className="about_left">
        <img className="lazyload about_portrait" srcSet={profile.avatar.fixed.srcWebp} alt="" />
        <h1> {profile.fullName} </h1>
        <h3> {profile.description} </h3>

        <Row className="about_left_icon justify-content-center">
          {profile.links.map(i =>
            <a style={{color:"unset", textDecoration: "none"}} target="_blank" rel="noreferrer" href={i.link}>
              <FontAwesomeIcon className="mx-2" icon={[i.group, i.icon]} size="2x"/>
            </a>
          )}
        </Row>
      </Col>
      <Col xs="12" sm="8" md="8" className="about_right">
        <ReactMarkdown source={profile.bio.bio} escapeHtml={false} />
      </Col>
    </Row>
  )
}

export const Tags = ({ tags: { group } }) => {
  return (
      <div className="mb-3">
          <h1>Теги</h1>
          {group.sort(sort).map(item => {
              const value = item.value
              return (<Link className="badge badge-secondary info-badge m-1" to={`/tag/${value.toLowerCase()}`}>
                  {value} <span className="badge badge-light">{item.pageInfo.itemCount}</span>
              </Link>)
          })}
      </div>
  )
}

export const Categories = ({ categories: { group } }) => {
    return (
        <div className="mb-3">
            <h1>Категории</h1>
            {group.sort(sort).map(item => {
                const value = item.value
                return (<Link className="badge badge-secondary info-badge m-1" to={`/category/${value.toLowerCase()}`}>
                    {value} <span className="badge badge-light">{item.pageInfo.itemCount}</span>
                </Link>)
            })}
        </div>
    )
}

export const RecentPosts = ({ posts: { edges } }) => {
  return (
    <Card className="single-card">
      <h2 className="single-title">Последние записи</h2>

      { edges.map(({node}) =>
        <div>
          <div className="ml-2">
            <blockquote className="self-elem-quote self-elem-quote-bg-blue mt-1" style={{backgroundColor:'#FFFFFF'}}>
              <Link to={'post/' + node.link}>
                <h3>{node.title}</h3>
              </Link>
              <PostInfo item={node}/>
            </blockquote>
          </div>
        </div>
      ) }
    </Card>
  )
}

export const PostInfo = ({ item }) => {
  return (
    <small>
      <span className="info-badge">
        <FontAwesomeIcon className="mr-2" icon={["far", "calendar"]} size="1x" />
        {item.publishedAt && moment(item.publishedAt).format("DD.MM.YYYY HH:mm")}
      </span>
      { item.categories && categoriesBlock(item.categories)}
      { item.tags && tagsBlock(item.tags)}
    </small>
  )
}

export const Pagination = ({ pageContext, prefix }) => {
  return (
    <ul className="pagination justify-content-center">
      {<li className={"page-item " + (!pageContext.previousPagePath && "disabled")}>
        <Link className="page-link" to={pageContext.previousPagePath} tabIndex="-1">&#171;</Link>
      </li>}
      { _.range(0, pageContext.numberOfPages).map(i => {
        const num = i + 1;
        const link = i === 0 ? prefix : prefix + "/" + num
        return (
          <li className={"page-item " + (pageContext.pageNumber === i && "active")}>
            <Link className="page-link" to={link}>{num}</Link>
          </li>
        )
      })
      }
      {<li className={"page-item " + (!pageContext.nextPagePath && "disabled")}>
        <Link className="page-link" to={pageContext.nextPagePath}>&#187;</Link>
      </li>
      }
    </ul>
  )
}

function categoriesBlock(categories) {
  return (
    <div>
      Категории: {categories.map(category =>
      <Link className="badge badge-secondary info-badge m-1" to={'category/' + category.toLowerCase()}>
        <FontAwesomeIcon className="mr-2" icon={["fas", "list-ul"]} size="1x" />{ category }
      </Link>)}
    </div>
  )
}

function tagsBlock(tags) {
  return (
    <div>
      Теги: {tags.map(tag =>
        <Link className="badge badge-secondary info-badge m-1" to={"tag/" + tag.toLowerCase()}>
          <FontAwesomeIcon className="mr-2" icon={["fas", "tag"]} size="1x" />{ tag }
        </Link>)}
    </div>
  )
}

function sort(o1, o2) {
  return o2.pageInfo.itemCount - o1.pageInfo.itemCount
}