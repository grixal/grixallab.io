import React, { PureComponent } from "react";

class ImgBlock extends PureComponent {
  render() {
    const {src, alt} = this.props
    return (<img src={src} class="img-fluid mx-auto d-block" alt={alt} />);
  }
}

export default ImgBlock