import React, { PureComponent } from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { darcula } from "react-syntax-highlighter/dist/esm/styles/prism";

class CodeBlock extends PureComponent {
  render() {
    const { language, value } = parse_meta(this.props);
    return (
      <div>
        <SyntaxHighlighter language={language} style={darcula}>
          {value}
        </SyntaxHighlighter>
        <br />
      </div>
    );
  }
}

function parse_meta(props) {
  const value = props.value
  if (!value.startsWith("%")) return props

  const metaIdx = value.indexOf("\n");
  return {
    language: value.substr(1, metaIdx - 1 ),
    value: value.substr(metaIdx + 1, value.length)
  }
}

export default CodeBlock