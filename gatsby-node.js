const path = require('path')
const { paginate } = require('gatsby-awesome-pagination')

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions
  return graphql(`{
  posts: allContentfulArticle {
    edges {
      node {
        link
      }
    }
  }
  works: allContentfulProject {
    edges {
      node {
        link
      }
    }
  }
  tags: allContentfulArticle {
    distinct(field: tags)
  }
  categories: allContentfulArticle {
    distinct(field: categories)
  }
}`).then(({data: {tags, categories, posts, works}}) => {
    //create Posts
    posts.edges.forEach(({node}) => {
      createPage({
        path: "post/" + node.link,
        component: path.resolve("./src/templates/post.js"),
        context: {
          slug: node.link
        }
      })
    })

    //create Works
    works.edges.forEach(({node}) => {
      createPage({
        path: "work/" + node.link,
        component: path.resolve("./src/templates/work.js"),
        context: {
          slug: node.link
        }
      })
    })

    //create Blog paginate
    paginate({
      createPage,
      items: posts.edges,
      itemsPerPage: 10,
      pathPrefix: '/blog',
      component: path.resolve('./src/templates/blog.js')
    })

    //create Tags
    tags.distinct.forEach(item => {
      createPage({
        path: "tag/" + item.toLowerCase(),
        component: path.resolve("./src/templates/post-list.js"),
        context: {
          slug: item,
          qualifier: {tags: {glob: item}}
        }
      })
    })

    //create Categories
    categories.distinct.forEach(item => {
      createPage({
        path: "category/" + item.toLowerCase(),
        component: path.resolve("./src/templates/post-list.js"),
        context: {
          slug: item,
          qualifier: {categories: {glob: item}}
        }
      })
    })
  })
}